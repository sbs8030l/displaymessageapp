jQuery(document).ready(function ($) {

  $(".btn_popup").trigger('click');

  /* Below code is mostly required in every project, so please don't remove it */
  $.ajaxSetup({
    headers: {
      "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
    }
  });

  $("#chosens-select")
    .change(function () {
      // $("#product_name").val($(this).text());
      $("#product_name").val($("#chosens-select option:selected").text());
    })
    .change();

  $("#delete_product").click(function () {
    var metaid = $(this).data("meta");
    var trigger = $(this).data("trigger");

    $.ajax({
      headers: {
        "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
      },
      method: "POST",
      url: "/delete_offer",
      data: {
        _token: $('meta[name="csrf-token"]').attr("content"),
        meta_id: metaid,
        trigger: trigger
      },
      success: function (data) {
        parent.location.reload();
      }
    });
    parent.location.reload();
  });
  $("#delete_timer").click(function () {
    var stock = $(this).data("stock");
    var timer = $(this).data("timer");
    var timerColor = $(this).data("color");
    var productId = $(this).data("trigger");

    $.ajax({
      headers: {
        "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
      },
      method: "POST",
      url: "/delete_timer",
      dataType: "JSON",
      data: {
        _token: $('meta[name="csrf-token"]').attr("content"),
        stock: stock,
        timer: timer,
        timerColor: timerColor,
        productId: productId
      },
      success: function (data) {
        alert(data);
        parent.location.reload();
      }
    });
    parent.location.reload();
  });

  //Handling tabs click event
  $("ul.tabs li").click(function () {
    var tab_id = $(this)
      .children()
      .attr("href");

    $("ul.tabs li").removeClass("active");
    $(".tab-content").removeClass("current");

    $(this).addClass("active");
    $(tab_id).addClass("current");
  });

});