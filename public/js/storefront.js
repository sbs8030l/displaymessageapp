var loadScript = function(url, callback) {
  /* JavaScript that will load the jQuery library on Google's CDN.
     We recommend this code: http://snipplr.com/view/18756/loadscript/.
     Once the jQuery library is loaded, the function passed as argument,
     callback, will be executed. */
};

var myAppJavaScript = function($) {
  /* Your app's JavaScript here.
     $ in this scope references the jQuery object we'll use.
     Don't use 'jQuery', or 'jQuery191', here. Use the dollar sign
     that was passed as argument.*/

  var months = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December"
  ];
  var now2 = new Date();
  // var newdate = new Date();
  // var thisMonth = months[now2.getMonth()];

  var message = make_message("message0");
  var message1 = make_message("message1");
  var message2 = make_message("message2");
  var message3 = make_message("message3");
  var message4 = make_message("message4");

  function make_message(class_name) {
    var now = new Date();
    var newdate = new Date();
    var today = now.getDate();
    var futureToday = newdate.getDate();
    var full_message = $("." + class_name).val();
    var customize_message = $(".message").val();

    var part1_message_pos = full_message.indexOf("--");
    var between2 = full_message.substring(0, part1_message_pos);
    var convert_to_int = parseInt(between2, 10);
    var convert_to_today = parseInt(today, 10);
    now.setDate(now.getDate() + convert_to_int);
    var between = now.getDate();
    var thisMonth = months[now.getMonth()];
    // console.log(convert_to_int + "1");
    // console.log(between);

    var part2_message_pos = full_message.indexOf("//");
    var too2 = full_message.substring(part1_message_pos + 2, part2_message_pos);
    var convert_to_int2 = parseInt(too2, 10);
    var convert_to_today2 = parseInt(today, 10);
    newdate.setDate(newdate.getDate() + convert_to_int2);
    var too = newdate.getDate();
    var thisMonth2 = months[newdate.getMonth()];
    // console.log(convert_to_int2 + "2");
    // console.log(too);

    var part3_message_pos = full_message.indexOf("---");
    var country = full_message.substring(
      part2_message_pos + 2,
      part3_message_pos
    );

    var toggle = full_message.substring(part3_message_pos + 3);

    if (toggle == "on") {
      if (customize_message !== "") {
        // customize_message.replace("{date}", "replace1");
        var c1 = customize_message.replace("{date}", between + " " + thisMonth);
        // customize_message.replace("{date2}", "replace2");
        var c2 = c1.replace("{date2}", too + " " + thisMonth2);
        var c3 = c2.replace("{country}", country);
        // customize_message.replace("{country}", "replace3");
        console.log(c3);
        return "<p>" + c3 + "</p>";
      } else {
        return (
          "<p> Estimated Arrival Between " +
          between +
          " " +
          thisMonth +
          " - " +
          too +
          " " +
          thisMonth2 +
          " to " +
          country +
          "</p>"
        );
      }
    } else {
      return "";
    }
  }

  // if (check !== "") {
  if (Shopify.theme.name == "Pop") {
    $(".product-single__desc").before(
      message + "" + message1 + "" + message2 + "" + message3 + "" + message4
    );
    // $("#addToCart-product-template").after(
    //   message + "" + message1 + "" + message2 + "" + message3 + "" + message4
    // );
    // if ($("#custom_cart").length != 0) {
    //   $("#custom_cart").after(
    //     message + " " + message1 + " " + message2 + " " + message3 + " " + message4
    //   );
    // }
    // $("#addToCart-product-template").remove();
  } else if (Shopify.theme.name == "Venture") {
    $(".product-single__description").before(
      message + "" + message1 + "" + message2 + "" + message3 + "" + message4
    );
    // $("#AddToCart-product-template").after(
    //   message + " " + message1 + " " + message2 + " " + message3 + " " + message4
    // );
    // if ($("#custom_cart").length != 0) {
    //   $("#custom_cart").after(
    //     message + " " + message1 + " " + message2 + " " + message3 + " " + message4
    //   );
    // }
    // $("#AddToCart-product-template").remove();
  } else if (Shopify.theme.name == "Minimal") {
    $(".product-description").before(
      message + "" + message1 + "" + message2 + "" + message3 + "" + message4
    );
    // $("#AddToCart").after(
    //   message + " " + message1 + " " + message2 + " " + message3 + " " + message4
    // );
    // if ($("#custom_cart").length != 0) {
    //   $("#custom_cart").after(
    //     message + " " + message1 + " " + message2 + " " + message3 + " " + message4
    //   );
    // }
    // $("#AddToCart").remove();
  } else if (Shopify.theme.name == "Brooklyn") {
    $(".product-single__description").before(
      message + "" + message1 + "" + message2 + "" + message3 + "" + message4
    );
    // $("#AddToCart--product-template").after(
    //   message + " " + message1 + " " + message2 + " " + message3 + " " + message4
    // );
    // if ($("#custom_cart").length != 0) {
    //   $("#custom_cart").after(
    //     message + " " + message1 + " " + message2 + " " + message3 + " " + message4
    //   );
    // }
    // $("#AddToCart--product-template").remove();
  } else if (Shopify.theme.name == "Narrative") {
    $(".product__description").before(
      message + "" + message1 + "" + message2 + "" + message3 + "" + message4
    );
    // $(".product__add-to-cart-button").after(
    //   message + " " + message1 + " " + message2 + " " + message3 + " " + message4
    // );
    // if ($("#custom_cart").length != 0) {
    //   $("#custom_cart").after(
    //     message + " " + message1 + " " + message2 + " " + message3 + " " + message4
    //   );
    // }
    // $(".product__add-to-cart-button").remove();
  } else if (Shopify.theme.name == "Supply") {
    $(".product-description").before(
      message + "" + message1 + "" + message2 + "" + message3 + "" + message4
    );
    // $("#addToCart-product-template").after(
    //   message + " " + message1 + " " + message2 + " " + message3 + " " + message4
    // );
    // if ($("#custom_cart").length != 0) {
    //   $("#custom_cart").after(
    //     message + " " + message1 + " " + message2 + " " + message3 + " " + message4
    //   );
    // }
    // $("#addToCart-product-template").remove();
  } else if (Shopify.theme.name == "Jumpstart") {
    $(".product-description").before(
      message + "" + message1 + "" + message2 + "" + message3 + "" + message4
    );
    // $("#AddToCart-product-template").after(
    //   message + " " + message1 + " " + message2 + " " + message3 + " " + message4
    // );
    // if ($("#custom_cart").length != 0) {
    //   $("#custom_cart").after(
    //     message + " " + message1 + " " + message2 + " " + message3 + " " + message4
    //   );
    // }
    // $("#AddToCart-product-template").remove();
  } else if (Shopify.theme.name == "Boundless") {
    $(".product-single__description").before(
      message + "" + message1 + "" + message2 + "" + message3 + "" + message4
    );
    // $("#AddToCart-product-template").after(
    //   message + " " + message1 + " " + message2 + " " + message3 + " " + message4
    // );
    // if ($("#custom_cart").length != 0) {
    //   $("#custom_cart").after(
    //     message + " " + message1 + " " + message2 + " " + message3 + " " + message4
    //   );
    // }
    // $("#AddToCart-product-template").remove();
  } else if (Shopify.theme.name == "Debut" || Shopify.theme.name == "debut") {
    $(".product-single__description").before(
      message + "" + message1 + "" + message2 + "" + message3 + "" + message4
    );
    // $("#AddToCart-product-template").after(
    //   message + " " + message1 + " " + message2 + " " + message3 + " " + message4
    // );
    // if ($("#custom_cart").length != 0) {
    //   $("#custom_cart").after(
    //     message + " " + message1 + " " + message2 + " " + message3 + " " + message4
    //   );
    // }
    // $("#AddToCart-product-template").remove();
  } else if (Shopify.theme.name == "Simple" || Shopify.theme.name == "simple") {
  }
};

if (typeof jQuery === "undefined" || parseFloat(jQuery.fn.jquery) < 1.7) {
  loadScript(
    "//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js",
    function() {
      jQuery191 = jQuery.noConflict(true);
      myAppJavaScript(jQuery191);
    }
  );
} else {
  myAppJavaScript(jQuery);
}
