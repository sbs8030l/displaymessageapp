<!DOCTYPE html>
<html>

<head>
    <title>Awesome Application</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <script src="//cdn.shopify.com/s/assets/external/app.js"></script>
    <link rel="stylesheet" type="text/css" href="{{ asset('css/uptown.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/app.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/chosen.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/color-picker.min.css') }}">
    @yield('styles')
    <script type="text/javascript">
        ShopifyApp.init({
            apiKey: "{{ env('SHOPIFY_APIKEY') }}",
            shopOrigin: '{{ "https://" . session("domain") }}'
        });
    </script>

    <script type="text/javascript">
        ShopifyApp.ready(function() {

            ShopifyApp.Bar.initialize({

                icon: '',
                title: 'Awesome Application',
                buttons: {
                    primary: {
                        label: 'Help',
                        message: 'Help'
                    }
                }

            });

        });
    </script>
</head>

<body>

    <main>
        <header>
            <h1>Shipping Details</h1>
            <h2>Headline text over your own background</h2>
        </header>
        <br>
        @yield('content')
        <footer>
            <article class="help">
                <span></span>
                <p>Learn more about <a href="#">%screen%</a> at the <a href="#">%company%</a> Help Center.</p>
            </article>
        </footer>
    </main>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
    <script src="{{  asset('js/app.js' )}}"></script>
    <script src="{{  asset('js/chosen.jquery.js' )}}"></script>
    <script src="{{  asset('js/init.js' )}}"></script>
    <script src="{{  asset('js/prism.js' )}}"></script>
    <script src="{{  asset('js/color-picker.min.js' )}}"></script>
    <script>
        function get_value($id = "") {
            var productname = $("#name-" + $id).text();
            var between = $("#between-" + $id).val();
            var too = $("#too-" + $id).val();
            var country = $("#country-" + $id).val();

            if (between >= 31 || too >= 31) {
                $(".error_addition").show();
                return false;
            }
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: 'POST',
                url: 'https://app.sbs8030l.xyz/shipping',
                data: {
                    'product_id': $id,
                    'productname': productname,
                    'between': between,
                    'too': too,
                    'country': country
                },
                success: function(data) {
                    $(".ajax_remove").html(data);
                }
            });
        }

        function submit_message() {
            var message = $("#message").val();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: 'POST',
                url: 'https://app.sbs8030l.xyz/message',
                data: {
                    'message': message,
                },
                success: function(data) {
                    $(".success_addition_message").show();
                }
            });
        }

        function toggle_ajax_function(id) {
            var e = $("#" + id);
            var metafieldID = e.data('id');
            if (e.is(":checked") == true) {

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: 'POST',
                    url: 'https://app.sbs8030l.xyz/change_toggle',
                    data: {
                        'metafieldId': metafieldID,
                        'toggle_check': '1'
                    },
                    success: function(data) {
                        $(".ajax_remove").html(data);
                    }
                });

            } else if (e.is(":checked") == false) {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: 'POST',
                    url: 'https://app.sbs8030l.xyz/change_toggle',
                    data: {
                        'metafieldId': metafieldID,
                        'toggle_check': '2'
                    },
                    success: function(data) {
                        $(".ajax_remove").html(data);
                    }
                });
            }

            // console.log("toggle working");
        }

        $(document).ready(function() {
            var picker2 = new CP(
                document.querySelector('input[name="replace_item_btn_color"]')
            );
            picker2.on("drag", function(color) {
                this.target.value = "#" + color;
            });

            $(".offer_button_nav").click(function() {
                console.log("Happen");
                $("#upsell_listing").fadeOut('slow', function() {
                    $(this).fadeIn('slow');
                });
            });

            $(".offer_button_nav1").click(function() {
                console.log("Happen");
                $("#upsell").fadeOut('slow', function() {
                    $(this).fadeIn('slow');
                });
            });
        });
    </script>
    @yield('scripts')
</body>

</html>