<article>
    <div class="columns tenth card">
        <h5>List of added shipping details</h5>
        <table>
            <thead>
                <tr>
                    <!-- <th>Toggle</th> -->
                    <th>Product</th>
                    <th>Date</th>
                    <th>Date2</th>
                    <th>Country</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <?php $product_name1; ?>
                @foreach($shipping_details_all as $shipping)
                <tr>
                    <!-- <td>
                        <label class="toogle_switch" style="display: none;">
                            @if($shipping->toggleCheck == "on")
                            <input type="checkbox" class="toggle_checkbox" id="toggle{{ $shipping->id }}" onchange='toggle_ajax_function("toggle{{ $shipping->id }}")' data-id="{{ $shipping->metafieldId }}" checked>
                            @else
                            <input type="checkbox" id="toggle{{ $shipping->id }}" class="toggle_checkbox" onchange='toggle_ajax_function("toggle{{ $shipping->id }}")' data-id="{{ $shipping->metafieldId }}">
                            @endif
                            <span class="toogle_slider toogle_round"></span>
                        </label>
                    </td> -->
                    <?php
                    $product_name = $shipping->productName;
                    if (empty($product_name1) || $product_name1 != $product_name) {
                        $product_name1 = $shipping->productName;
                        $product_name = $shipping->productName;
                        ?>
                        <td>{{ $shipping->productName }}</td>
                    <?php } else {
                    ?>
                        <td></td>
                    <?php
                } ?>
                    <td>{{ $shipping->between }}</td>
                    <td>{{ $shipping->to }}</td>
                    <td>{{ $shipping->country }}</td>
                    <td><a href="https://app.sbs8030l.xyz/delete?shipping_id={{ $shipping->id }}&metafieldid={{ $shipping->metafieldId }}&productid={{ $shipping->productId }}" class="button warning">Trash</a></td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    <div class="columns two card secondary">
        <p>This card is slightly dimmed to give the primary card more visual importance</p>
    </div>
</article>