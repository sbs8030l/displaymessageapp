<?php

namespace App\Objects;

use Log;

use Oseintow\Shopify\Facades\Shopify;

class ShopifyWebhook
{

    public static function registerAppUninstallWebhook()
    {
        $postData = [

            "topic" => "app/uninstalled",
            "address" => config('app.url') . '/shopify/webhook/app_uninstall',
            "format" => "json"

        ];

        Shopify::setShopUrl(session('myshopifyDomain'))
            ->setAccessToken(session('accessToken'))
            ->post('admin/webhooks.json', ["webhook" => $postData]);

        $postData2 = [

            "topic" => "themes/publish",
            "address" => config('app.url') . 'reload',
            "format" => "json"

        ];
        $response_1 = Shopify::setShopUrl(session('myshopifyDomain'))
            ->setAccessToken(session('accessToken'))
            ->post('admin/webhooks.json', ["webhook" => $postData2]);
        Log::info($response_1);


        $postData3 = [
            "topic" => "products/update",
            "address" => config('app.url') . 'update_product',
            "format" => "json"
        ];
        $response_2 = Shopify::setShopUrl(session('myshopifyDomain'))
            ->setAccessToken(session('accessToken'))
            ->post('admin/webhooks.json', ["webhook" => $postData3]);
        Log::info($response_2);
    }
}
