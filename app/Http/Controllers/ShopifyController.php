<?php

namespace App\Http\Controllers;

use DB;
use Log;
use App\Shop;
use App\Setting;
use App\Objects\ScriptTag;
use Illuminate\Http\Request;
use Oseintow\Shopify\Shopify;
use App\Objects\ShopifyWebhook;
use Oseintow\Shopify\Exceptions\ShopifyApiException;
use App\ShopInfo;


class ShopifyController extends Controller
{
	protected $shopify;

	function __construct(Shopify $shopify)
	{
		$this->shopify = $shopify;
	}

	public function access(Request $request)
	{

		$shopUrl = $request->shop;
		// dd($shopUrl);
		if ($shopUrl) {
			$shop = Shop::where('myshopify_domain', $shopUrl)->first();
			// dd($shop);
			if ($shop) {
				session([

					'shopifyId' => $shop->shopify_id,
					'myshopifyDomain' => $shop->myshopify_domain,
					'accessToken' => $shop->access_token

				]);

				$shopProducts = $this->shopify->setShopUrl($shop->myshopify_domain)
					->setAccessToken($shop->access_token)
					->get('admin/products.json', ['limit' => 250, 'page' => 1]);

				$shipping_details = DB::table('shipping_details')->select('id', 'productName', 'productId', 'between', 'to', 'country', 'toggleCheck', 'metafieldId')->where('shopifyStoreId', $shop->shopify_id)->get();
				return view('home.index', ['shop' => $shop, 'settings' => $shop->settings, 'shop_products' => $shopProducts, 'shipping_details' => $shipping_details, 'success' => '0']);
			} else {
				$shopify = $this->shopify->setShopUrl($shopUrl);
				return redirect()->to($shopify->getAuthorizeUrl(config('shopify.scope'), config('shopify.redirect_uri')));
			}
		} else {
			abort(404);
		}
	}

	public function callback(Request $request)
	{
		$queryString = $request->getQueryString();

		if ($this->shopify->verifyRequest($queryString)) {

			$shopUrl = $request->shop;
			try {
				$accessToken = $this->shopify->setShopUrl($shopUrl)->getAccessToken($request->code);

				$shopResponse = $this->shopify->setShopUrl($shopUrl)
					->setAccessToken($accessToken)
					->get('admin/shop.json');
				if ($shopResponse) {
					session([
						'shopifyId' => $shopResponse['id'],
						'myshopifyDomain' => $shopUrl,
						'accessToken' => $accessToken
					]);

					$shop = $this->createShop($shopResponse);
					$this->createDefaultSettings($shop);
					$this->storeShopInfo($shopResponse, $shop->id);

					ShopifyWebhook::registerAppUninstallWebhook();

					if (config('shopify.billing_enabled')) {
						return redirect()->route('billing.charge');
					}

					ScriptTag::register();
					$this->include_template_files();
					return redirect("https://{$shopUrl}/admin/apps/displaymessage");
				}
			} catch (ShopifyApiException $e) {
				Log::critical("Installation Callback exception.", ['message' => $e->getMessage(), 'shop' => $shopUrl]);
				abort(500);
			}
		} else {
			abort(500, "Hmm, Something doesn't look right.");
		}
	}

	protected function createShop($data)
	{
		return Shop::create([
			'shopify_id' => $data['id'],
			'myshopify_domain' => $data['myshopify_domain'],
			'access_token' => session('accessToken')
		]);
	}

	protected function createDefaultSettings($shop)
	{
		return $settings = Setting::create([
			'enabled' => 1,
			'shop_id' => $shop->id,
			'myshopify_domain' => $shop->myshopify_domain
		]);
	}

	protected function storeShopInfo($data, $shopId)
	{
		unset($data['id']);
		$data['shop_id'] = $shopId;
		return ShopInfo::create($data->toArray());
	}

	public function reload_theme(Request $request)
	{
		$data = $request->getContent();
		$hmacHeader = $request->server('HTTP_X_SHOPIFY_HMAC_SHA256');

		if (Shopify::verifyWebHook($data, $hmacHeader)) {
			$store_id = $data['theme_store_id'];
			$shop_info = DB::Table('shops')->where('shopify_id', $store_id);
			session([
				'shopifyId' => $shop_info['0']->shopify_id,
				'myshopifyDomain' => $shop_info['0']->myshopify_domain,
				'accessToken' => $shop_info['0']->access_token
			]);
		}

		$this->include_template_files();
		Log::info('Webhook Theme publish Request verified and Handled.');
		return new Response('Webhook Handled', 200);
	}

	public function update_product(Request $request)
	{
		return new Response('Webhook Handled', 200);
		$data = $request->getContent();
		$hmacHeader = $request->server('HTTP_X_SHOPIFY_HMAC_SHA256');

		if (Shopify::verifyWebHook($data, $hmacHeader)) {
			Log::info("Hook Called");
			$payload = json_decode($data, true);
			$product_id = $payload['id'];
			$product_name = $payload['handle'];

			$update_name = $product_id . $product_name;

			$shopifyId = session('shopifyId');

			$update = DB::table('upsell_data')
				->where('shopifyStoreId', $shopifyId)
				->where('triggerProduct', $product_id)
				->update(['triggerProduct' => $update_name, 'updated_at' => date('Y-m-d H:i:s')]);
		}
		return new Response('Webhook Handled', 200);
	}

	//Mandatory Webhooks

	public function customer_redact()
	{
		Log::info('Webhook Request verified and Handled. customer_redact');
		return new Response('Webhook Handled', 200);
	}

	public function shop_redact()
	{
		Log::info('Webhook Request verified and Handled. shop_redact');
		return new Response('Webhook Handled', 200);
	}

	public function data_request()
	{
		Log::info('Webhook Request verified and Handled. data_request');
		return new Response('Webhook Handled', 200);
	}

	//End Mandatory Webhooks
	//Storing Meta Values in database for
	//Showing the listing on the backend of the app

	public function store_message($metafieldId = "", $message = "", $storeId  = "")
	{
		$shopUrl = session('myshopifyDomain');
		$accessToken = session('accessToken');
		$shopifyId = session('shopifyId');

		$shipping_detail_update = DB::Table('message_details')->where('shopifyStoreId', $shopifyId)->first();
		if ($shipping_detail_update) {
			$update = DB::table('message_details')
				->where('shopifyStoreId', $shopifyId)
				->update([
					'message' => $message,
					'updated_at' => date('Y-m-d H:i:s')
				]);
		} else {
			$id = DB::table('message_details')->insertGetId([
				'message' => $message,
				'shopifyStoreId' => $shopifyId,
				'metafieldId' => $metafieldId,
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			]);
		}
	}

	public function store_metafields($between = "", $too = "", $country = "", $product_id = "", $product_name = "", $metafieldNewId = "", $value_toggle = "")
	{
		$shopUrl = session('myshopifyDomain');
		$accessToken = session('accessToken');
		$shopifyId = session('shopifyId');

		$shipping_detail_update = DB::Table('shipping_details')->where('shopifyStoreId', $shopifyId)
			->where('productId', $product_id)->where('country', $country)->first();
		if ($shipping_detail_update) {
			$update = DB::table('shipping_details')
				->where('shopifyStoreId', $shopifyId)
				->where('productId', $product_id)
				->where('country', $country)
				->update([
					'productName' => $product_name,
					'between' => $between,
					'to' => $too,
					'toggleCheck' => $value_toggle,
					'updated_at' => date('Y-m-d H:i:s')
				]);
		} else {
			$id = DB::table('shipping_details')->insertGetId([
				'shopifyStoreId' => $shopifyId,
				'productId' => $product_id,
				'productName' => $product_name,
				'metafieldId' => $metafieldNewId,
				'between' => $between,
				'to' => $too,
				'toggleCheck' => $value_toggle,
				'country' => $country,
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			]);
		}
	}

	public function add_shipping_time()
	{
		$shopUrl = session('myshopifyDomain');
		$accessToken = session('accessToken');
		$shopifyId = session('shopifyId');

		$between = $_POST['between'];
		$too = $_POST['too'];
		$country = $_POST['country'];
		$product_id = $_POST['product_id'];
		$product_name = $_POST['productname'];
		$toggle_check = 1;
		$value_toggle = null;

		if ($toggle_check = 1) {
			$value_toggle = "on";
		} else {
			$value_toggle = "off";
		}

		$shipping_detail_metaid = DB::Table('shipping_details')->select('metafieldId')
			->where('shopifyStoreId', $shopifyId)
			->where('productId', $product_id)
			->where('country', $country)->get();
		$counting = count($shipping_detail_metaid);
		if ($counting) {

			$metafieldsId = $shipping_detail_metaid['0']->metafieldId;;
			$postData = [
				"id" => "shipping_identity",
				"value" => "$between--$too//$country---$value_toggle",
				"value_type" => "string"
			];
			$data2 = $this->shopify->setShopUrl($shopUrl)
				->setAccessToken($accessToken)
				->put("/admin/products/" . $product_id . "/metafields/" . $metafieldsId . ".json", ['metafield' => $postData]);

			$this->store_metafields($between, $too, $country, $product_id, $product_name, $value_toggle);
			$shipping_details_all = DB::table('shipping_details')->where('shopifyStoreId', $shopifyId)->get();
			return view('home.shipping-table')->with("shipping_details_all", $shipping_details_all);
		} else {
			$shipping_detail_metaid2 = DB::Table('shipping_details')->select('metafieldId')
				->where('productId', $product_id)->get();
			$counting2 = count($shipping_detail_metaid2);
			// return $counting2;
			$postData1 = [
				"namespace" => "shipping_identity",
				"key" => "message$counting2",
				"value" => "$between--$too//$country---$value_toggle",
				"value_type" => "string"
			];
			// return $postData1;
			$data1 = $this->shopify->setShopUrl($shopUrl)
				->setAccessToken($accessToken)
				->post("/admin/products/" . $product_id . "/metafields.json", ['metafield' => $postData1]);
			$metafieldNewId = $data1['id'];

			$this->store_metafields($between, $too, $country, $product_id, $product_name, $metafieldNewId, $value_toggle);
			$shipping_details_all = DB::table('shipping_details')->where('shopifyStoreId', $shopifyId)->get();

			return view('home.shipping-table')->with("shipping_details_all", $shipping_details_all);
		}
	}

	public function add_message()
	{
		$shopUrl = session('myshopifyDomain');
		$accessToken = session('accessToken');
		$shopifyId = session('shopifyId');

		$message = $_POST['message'];

		$postData = [
			"namespace" => "shipping_identity",
			"key" => "message",
			"value" => "$message",
			"value_type" => "string"
		];
		$data2 = $this->shopify->setShopUrl($shopUrl)
			->setAccessToken($accessToken)
			->post("/admin/api/2019-04/metafields.json", ['metafield' => $postData]);
		$metafieldid = $data2['id'];
		$this->store_message($metafieldid, $message, $shopifyId);
		// $shipping_details_all = DB::table('shipping_details')->where('shopifyStoreId', $shopifyId)->get();
		// return view('home.shipping-table')->with("shipping_details_all", $shipping_details_all);
	}

	public function change_toggle()
	{
		$metafieldId = $_POST['metafieldId'];
		$toggle_check = $_POST['toggle_check'];
		$toggle_value = null;
		if ($toggle_check == 1) {
			$toggle_value = "on";
		} else {
			$toggle_value = "off";
		}

		$shopUrl = session('myshopifyDomain');
		$accessToken = session('accessToken');
		$shopifyId = session('shopifyId');

		$shipping_detail_update = DB::Table('shipping_details')->where('shopifyStoreId', $shopifyId)
			->where('metafieldID', $metafieldId)->first();

		$country = $shipping_detail_update->country;
		$too = $shipping_detail_update->to;
		$between = $shipping_detail_update->between;
		$product_id = $shipping_detail_update->productId;

		if ($shipping_detail_update) {

			$postData = [
				"id" => "shipping_identity",
				"value" => "$between--$too//$country---$toggle_value",
				"value_type" => "string"
			];
			$data2 = $this->shopify->setShopUrl($shopUrl)
				->setAccessToken($accessToken)
				->put("/admin/products/" . $product_id . "/metafields/" . $metafieldId . ".json", ['metafield' => $postData]);

			$update = DB::table('shipping_details')
				->where('shopifyStoreId', $shopifyId)
				->where('metafieldID', $metafieldId)
				->update([
					'toggleCheck' => $toggle_value,
					'updated_at' => date('Y-m-d H:i:s')
				]);
			if ($update) {
				$shipping_details_all = DB::table('shipping_details')->where('shopifyStoreId', $shopifyId)->get();
				return view('home.shipping-table')->with("shipping_details_all", $shipping_details_all);
			}
		}
	}

	//Delete function which deletes the shipping

	public function delete_shipping()
	{
		$shopUrl = session('myshopifyDomain');
		$accessToken = session('accessToken');
		$shopifyId = session('shopifyId');

		$shipping = $_GET['shipping_id'];
		$metafieldid = $_GET['metafieldid'];
		$productid = $_GET['productid'];
		try {
			$deleteMetafield = $this->shopify->setShopUrl($shopUrl)
				->setAccessToken($accessToken)
				->delete("/admin/api/2019-04/products/$productid/metafields/$metafieldid.json");
			// dd($deleteMetafield);
			if ($deleteMetafield) {
				$deleting = DB::table('shipping_details')->where('id', $shipping)->delete();
			}
		} catch (ShopifyApiException $e) {
			$deleting = DB::table('shipping_details')->where('id', $shipping)->delete();
		}
		$shop = Shop::where('myshopify_domain', $shopUrl)->first();
		$shopProducts = $this->shopify->setShopUrl($shopUrl)
			->setAccessToken($accessToken)
			->get('admin/products.json', ['limit' => 250, 'page' => 1]);
		$shipping_details = DB::table('shipping_details')->select('id', 'productName', 'productId', 'between', 'to', 'country', 'toggleCheck', 'metafieldId')->where('shopifyStoreId', $shopifyId)->get();
		return view('home.index', ['shop' => $shop, 'settings' => $shop->settings, 'shop_products' => $shopProducts, 'shipping_details' => $shipping_details, 'success' => '1']);
	}


	//This function include our custom template in the product.liquid.
	// Two function include_template_files and update_template_file
	// Are responsible for appending this code to product.liquid
	public function include_template_files()
	{
		$shopUrl = session('myshopifyDomain');
		$accessToken = session('accessToken');
		$data = $this->shopify->setShopUrl($shopUrl)->setAccessToken($accessToken)->get('admin/themes.json');
		$objection = json_decode($data);
		$name_check = "";
		$theme_id = "";
		foreach ($data as $datas) {
			if ($datas->role == "main") {
				$theme_id = $datas->id;
				$name_check = $datas->name;
			}
		}
		$template_name = 'templates/product.liquid';
		$server_template = $this->shopify->setShopUrl($shopUrl)
			->setAccessToken($accessToken)
			->get("/admin/themes/" . $theme_id . "/assets.json", ["asset[key]" => "$template_name", "theme_id" => $theme_id]);
		$view = $server_template['value'];
		$view .= "{% assign event_identifier = product.metafields.shipping_identity %}
		<input type='hidden' value='{{ event_identifier['message0'] }}' id='event_identifier' class='message0' />
		<input type='hidden' value='{{ event_identifier['message1'] }}' id='event_identifier' class='message1' />
        <input type='hidden' value='{{ event_identifier['message2'] }}' id='event_identifier' class='message2' />
		<input type='hidden' value='{{ event_identifier['message3'] }}' id='event_identifier' class='message3' />
		<input type='hidden' value='{{ event_identifier['message4'] }}' id='event_identifier' class='message4' />
		<input type='hidden' value='{{ product.variants.first.id }}' id='product_id' class='product' />
		<input type='hidden' value='{{ product.id }}' id='product_id_real' class='product' />
";

		$this->update_templete_files($view, $shopUrl, $accessToken, $theme_id);
	}


	public function update_templete_files($view, $shopUrl, $accessToken, $theme_id)
	{
		$postData = [
			"key" => "templates/product.liquid",
			"value" => $view
		];
		$data = $this->shopify->setShopUrl($shopUrl)->setAccessToken($accessToken)->put('/admin/themes/' . $theme_id . '/assets.json', ['asset' => $postData]);
		// dd($data);
	}
}
