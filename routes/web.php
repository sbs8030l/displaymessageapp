<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('/', function () {
    return view('welcome');
});


Route::group(['prefix' => 'shopify'], function () {

    Route::get('access', 'ShopifyController@access')->name('shopify.access');
    Route::get('callback', 'ShopifyController@callback')->name('shopify.callback');
    Route::post('webhook/app_uninstall', 'WebhookController@app_uninstall');
});

Route::group(['prefix' => 'billing'], function () {

    Route::get('/', 'BillingController@index')->name('billing.index');
    Route::get('charge', 'BillingController@charge')->name('billing.charge');
    Route::get('callback', 'BillingController@callback')->name('billing.callback');
    Route::get('declined', 'BillingController@declined')->name('billing.declined');
});
Route::get('loading', 'ShopifyController@load_offer');
Route::post('reload', 'ShopifyController@reload_theme');
Route::post('update_product', 'ShopifyController@update_product');

Route::post('customer/data_request', 'ShopifyController@data_request');
Route::post('shop/redact', 'ShopifyController@shop_redact');
Route::post('customers/redact', 'ShopifyController@customer_redact');

Route::post('shipping', 'ShopifyController@add_shipping_time');
Route::post('message', 'ShopifyController@add_message');
Route::get('delete', 'ShopifyController@delete_shipping');
Route::post('change_toggle', 'ShopifyController@change_toggle');
